from django.db import models

# Create your models here.
class Borrower(models.Model):
    borrower_id=models.IntegerField()
    Accession_number=models.IntegerField()
    Author_of_book=models.CharField(max_length=70)
    Title_of_book=models.CharField(max_length=70)

class Student(models.Model):
    student_id=models.IntegerField()
    student_name=models.CharField(max_length=70)
    student_borrowbook=models.CharField(max_length=70)

class Staff(models.Model):
    Staff_Id=models.IntegerField()
    Staff_Name=models.CharField(max_length=70)
    Designation=models.CharField(max_length=70)