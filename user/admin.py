from django.contrib import admin
from user.models import Borrower,Student,Staff

# Register your models here.
admin.site.register(Borrower)
admin.site.register(Student)
admin.site.register(Staff)
