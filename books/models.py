from django.db import models

# Create your models here.
class Book(models.Model):
    ISBN_Code=models.IntegerField()
    Publication_year=models.IntegerField()
    Book_Title=models.CharField(max_length=70)
    Language=models.CharField(max_length=70)

class Binding(models.Model):
    Binding_id=models.IntegerField()
    Binding_Name=models.CharField(max_length=70)

class Category(models.Model):
    Category_Id=models.IntegerField()
    Category_Name=models.CharField(max_length=70)

class Shelf(models.Model):
    Shelf_Id=models.IntegerField()
    Shelf_no=models.IntegerField()
    Floor_no=models.IntegerField()
    


    