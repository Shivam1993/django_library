from django.contrib import admin
from books.models import Book,Binding,Category,Shelf

# Register your models here.
admin.site.register(Book)
admin.site.register(Binding)
admin.site.register(Category)
admin.site.register(Shelf)



